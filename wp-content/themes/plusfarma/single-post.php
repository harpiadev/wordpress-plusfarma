<?php  get_header(); ?>


	<main id="single-post">
		<div class="container interna">
			 <?php the_post(); ?>

			<h1 class="text-center"><?php the_title();?></h1>

			<?php if ( has_post_thumbnail() ) : ?>
				 <div class="col s12 m5 text-center fl">
                    <img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id(get_the_id()), 'large' )[0];?>" alt="<?php echo get_the_title();?>">
                </div>
            <?php endif; ?>

		 	<?php the_content();?>
		</div>
	</main>



<?php get_footer(); ?>