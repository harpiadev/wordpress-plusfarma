<?php get_header(); ?>

	<main id="galeria">

		<?php
	    # Albuns
	    $albuns = new WP_Query(
	        array(
	            'post_type' => 'album',
	            'posts_per_page' => -1
	        )
	    );

	    if ($albuns->have_posts()) :
	    ?>

		<div class="container interna">
			<h1 class="title"><span>GALERIA DE IMAGENS</span></h1>
			<div class="row">
				<?php
	            while($albuns->have_posts()) :
	                $albuns->the_post();

	            	// get custom fields
	            	$id = get_the_id();
	             	$link = get_field('link');
	             	$descricao = get_field('descricao');
	            ?>
				<div class="col s12 m6 l4">
					<a href="<?php the_permalink(); ?>">
						<?php if (has_post_thumbnail()) : ?>
						<figure>
							<?php the_post_thumbnail('medium_large', array('alt' => get_the_title()));?>
						<?php endif; ?>
							<figcaption>
								<p>
									<span><?php the_title();?></span>
								</p>
							</figcaption>
						</figure>
						<p><?php echo $descricao ?></p>
					</a>

				</div>
			<?php endwhile; ?>
			</div>
		<?php endif; ?>
		</div>
	</main>

<?php get_footer(); ?>
