$(".button-collapse").sideNav();
$(".dropdown-button").dropdown();
$('select').material_select();
// $(document).ready(function() {
//   });

$('.search-bar').hide();
$( ".btn-search" ).click(function() {
	 $('.search-bar').slideToggle("fast");
});


// Mascara para o formulario (data e telefone)
$(document).ready(function(){
  $('.date').mask('00/00/0000');
  $('.phone_with_ddd').mask('(00) 00000-0000');
  $('.phone_with_fixed_ddd').mask('(00) 0000-0000');
  $('.CPF').mask('000.000.000-00');
  $('.CNPJ').mask('00.000.000/0000-00');

});


/**
 * Alterar tamanho da imagem dos fornecedores de acordo com o tamanho da página
 =============================================================================== */

var $fornecedoresContainer = $('#home .marcas .container-marcas .template-marcas');
var resizeFornecedoresIcons = function() {
	var widthIcon = function() {
		var width = $('#home .marcas .container-marcas .template-marcas').width(),
			qtd;

		if (width >= 900) {
			qtd = 5;
		} else if (width >= 870) {
			qtd = 4;
		} else if (width >= 650) {
			qtd = 4;
		} else {
			qtd = 2;
		}

		return parseInt(width/qtd);
	}

	$('#responsive-fornecedores').remove();

	var style = document.createElement('style');
	style.id = 'responsive-fornecedores';
	style.type = 'text/css';
	style.innerHTML = '#home .marcas .container-marcas .template-marcas .item { width: ' + widthIcon() + 'px !important; }';
	document.getElementsByTagName('head')[0].appendChild(style);
}

if ($fornecedoresContainer.length > 0) {
	resizeFornecedoresIcons();
}




/**
 * Alterar endereço do mapa em contatos
 =============================================================================== */
var $endereco = $('.mapa-endereco');
var $mapa = $('.mapa-google');

 $('.mapa-cidades').on('change', function() {
	var result = $(this).val();
	
	var adress = ['Rua Barão do Rio Branco, 28 - Pan Americano, Fortaleza - CE', 'Av. Zequinha Freire, 1365 - Santa Isabel, Teresina - PI'];
	var map = [
		"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.3376318210567!2d-38.533536485241044!3d-3.736401897281614!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c74901241c82f5%3A0x5c7217b668205dce!2sPlusfarma+Produtos+Farmac%C3%AAuticos!5e0!3m2!1spt-BR!2sbr!4v1479841082352",
		"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15896.95402927501!2d-42.75621828796407!3d-5.0650485771394465!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x78e3a542b3712cd%3A0x15a0e3b232579919!2sPlusfarma!5e0!3m2!1spt-BR!2sbr!4v1479841328606"
	];
	$endereco.text(adress[result]);
	$mapa.attr("src", map[result]);

 });
