  <?php 
   

    $type = null;
    if ($_SERVER[ "REQUEST_METHOD" ] == "POST") {
        require __DIR__ . "/../../vendor/autoload.php";

        $type = $_POST[ "type" ];
        $error = null;

        if ($type == 'xml') {
            $xml = $_FILES[ "xml" ];
            
            try {
                $fileName = $xml['name'];
                $extension = end(explode('.', $fileName));

                if ($extension !== 'xml') {
                    throw new \Exception("Envie um arquivo XML!");
                }

                $docxml = \NFePHP\Common\Files\FilesFolders::readFile($xml[ "tmp_name" ]);
                $danfe = new \NFePHP\Extras\Danfe($docxml, 'P', 'A4', 'nfe/images/riosfarma.jpg', 'I', '');
                $id = $danfe->montaDANFE();

                header('Content-Disposition: inline; filename="danfe.pdf"');
                header("Content-type: application/pdf");
                echo $danfe->printDANFE($id.'.pdf', 'S');
                exit;
            } catch (\Exception $e) {
                $error = $e->getMessage();
            }
        } else if ($type == 'nfe') {
            $form = true;

            try {
                if (empty($_POST['chave_nfe'])) {
                    throw new \Exception("Informe o número da nota!");
                }

                // check if nfe number was sent
                $nfe = str_replace(' ', '', $_POST['chave_nfe']);

                if (strlen($nfe) !== 44) {
                    throw new \Exception("O número da nota deve conter 44 números.");
                }

                // incldue the class to craw data
                require "admin/NotaFiscalCrawler.php";

                // instance
                $crawler = new NotaFiscalCrawler;

                // check captcha
                if (isset($_POST['captcha'])) {
                    if ($crawler->view($_POST, 'chave_nfe', 'captcha')) {
                        $pdfLink = "data:application/pdf;base64," . $crawler->pdfContent;
                        // hide form
                        $form = false;
                        header('Content-Disposition: inline; filename="danfe.pdf"');
                        header("Content-type: application/pdf");
                        echo base64_decode($crawler->pdfContent);
                        exit;
                    }
                }

                if ($form) {
                    // get fields
                    $fields = $crawler->getViewFields();
                    $captcha = true;
                    $cookie = base64_encode($crawler->getCookieContent());
                }
            } catch (\Exception $e) {
                $error = $e->getMessage();
            }
        }
    }

    get_header();
    ?>
	<main class="formulario" id="nfe">
		<div class="container interna">
			<h1 class="title"><span>Consulta NF-E</span></h1>
			
			    <div class="row">
			    	<div class="col s12 m5 box">
						<p class="subtitulo">Visualizar Danfe por chave de acesso</p>
						<form id="form-nf" method="post" class="form-nf<?php echo ($type == 'nfe' && $form === false ? ' hidden' : '');?>"<?php echo isset($captcha) ? ' target="_blank"' : "";?>>
						<?php if (isset($captcha)) : ?>
							<input type="hidden" name="json" value="<?php echo base64_encode(serialize($fields));?>">
                            <input type="hidden" name="cookie" value="<?php echo $cookie;?>">

                            <div class="form-group text-center"><img src="data:image/png;base64,<?php echo $fields->img_captcha;?>" alt="Captcha"></div>
                                <div class="form-group">
                                    <label for="field-captcha">Validação</label>
                                    <input type="text" class="form-control" name="captcha" id="field-captcha">
                                </div>
                            <?php endif; ?>

                            <input type="hidden" name="type" value="nfe">
					        
					        <div class="input-field">
					          	<input type="text" placeholder="Chave de acesso" name="chave_nfe" id="field-nf" value="<?php echo isset($captcha) ? $nfe : null;?>">
					        </div>
					        <button class="btn submit btn-rounded waves-effect color-light col s12 m4">Visualizar</button>

<!-- 
					        <div class="result-danfe<?php echo ($type == 'nfe' && $form === false ? '' : ' hidden');?>">
                            	<div class=""><a href="<?php echo $pdfLink;?>" id="pdf-download" class="btn" target="_blank">Visualizar DANFE</a></div>
                        	</div> -->
				        </form>
			    	</div>

			    	<div class="col s12 m5 offset-m2 box">
                        <?php if($type == 'xml' && $error) : ?>
                            <div class="alert alert-danger">
                                <?php echo $error;?>
                            </div>
                        <?php endif; ?>
			    		<p class="subtitulo">Visualizar Danfe por XML</p>
			    		<form method="post" class="form-nf<?php echo ($type == 'xml' && !$error ? ' hide' : '');?>" enctype="multipart/form-data" target = "_blank">
                            <input type="hidden" name="type" value="xml">
				    		<div class="file-field input-field">
						      	<div class="btn color-light">
						        	<span>Arquivo</span>
						        	<input type="file" name="xml">
						      	</div>
						      	<div class="file-path-wrapper">
						        	<input class="file-path validate" type="text" placeholder="nenhum arquivo selecionado">
						      	</div>
						    </div>
						    <button class="btn submit btn-rounded waves-effect color-light col s12 m4">Visualizar</button>
					    </form>
                        <div class="result-danfe<?php echo ($type == 'xml' && !$error ? '' : ' hide');?>">
                            <div class="form-group">
                                <a href="<?php echo $url;?>" target="_blank" class="btn btn-block btn-orange">Visualizar DANFE</a>
                            </div>
                        </div>
			    	</div>
			    </div>
		</div>
	</main>

<?php get_footer(); ?>