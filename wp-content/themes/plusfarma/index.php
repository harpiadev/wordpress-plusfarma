<?php get_header();
	/*
	Template Name: Página inicial
	*/
?>



	<section id="slider-home">
		<?php
	    # Banners
	    $banners = new WP_Query(
	        array(
	            'post_type' => 'banner',
	            'posts_per_page' => -1
	        )
	    );

	    if ($banners->have_posts()) :
	    ?>

		<div id="wrap-slider" class="home container-with-arrow">
			<a href="javascript:;" class="arrow left"><i class="fa fa-angle-left"></i></a>

			<div id="slider" class="cycle-slideshow" data-cycle-slides="> div" data-cycle-fx="scrollHorz" data-cycle-timeout="4000" data-cycle-slides=".item" data-cycle-next=".home .right" data-cycle-prev=".home .left">

				<?php
	            while($banners->have_posts()) :
	                $banners->the_post();

	            	// get custom fields
	            	$id = get_the_id();
	             	$link = get_field('link');
	            ?>
				<div>
					<a href="<?php echo $link;?>" class="item">
						<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($id) );?>" alt="<?php echo get_the_title();?>">
					</a>
				</div>
				<?php endwhile; ?>
			<?php endif; ?>
			</div>
			<a href="javascript:;" class="arrow right"><i class="fa fa-angle-right"></i></a>
		</div>
	</section>



	<main id="home">

		<?php
	    # Postagens
	    $posts = new WP_Query(
	        array(
	            'post_type' => 'post',
	            'posts_per_page' => 3,
	            'order' => 'DESC',
	            'orderby' => 'date'
	        )
	    );

	    if ($posts->have_posts()) :
	    ?>

		<section class="container interna" id="noticias">
			<h1 class="title"><span>NOTÍCIAS</span></h1>
			<div class="row">
				<?php
					while ( $posts->have_posts()) :
					$posts->the_post();
				?>
					<div class="col s12 m6 l4">
						<a href="<?php the_permalink(); ?>">
							<?php if (has_post_thumbnail()) : ?>
							<figure>
								<?php the_post_thumbnail('medium', array('alt' => get_the_title()));?>
							<?php endif; ?>
								<figcaption>
									<span><?php the_title();?></span>
								</figcaption>
							</figure>
							<p><?php echo get_excerpt(100);?></p>
						</a>

					</div>
				<?php endwhile ?>
			</div>
		</section>
		<!-- /#noticias -->
		<?php endif; ?>

		<section id="como-comprar">
			<div class="container interna">
				<h1 class="title"><span>FORMAS DE COMPRAR</span></h1>
				<div class="row">
					<div class="col s12 m6 l3">
						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/televendas2.png" alt="Televendas" class="responsive-img">
						<p class="subtitle">TELEVENDAS</p>
						<p>Nosso televendas está á sua disposição de segunda á sexta, das 8:00hs as 18:00hs, sempre com presteza e excelentes ofertas, nos diferenciando assim do mercado.</p>
					</div>
					<div class="col s12 m6 l3">
						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/compra-direta.png" alt="Venda direta" class="responsive-img">
						<p class="subtitle">VENDA DIRETA</p>
						<p>O GRUPO EMMA conta com um time de 120 representantes, atuando em todo o NORDESTE, com eficiência e eficácia. Todos sao equipados com tablets, gerando comodidade e rapidez no atendimento.</p>
					</div>
					<div class="col s12 m6 l3">
						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/pedido-eletronico.png" alt="Pedido Eletrônico" class="responsive-img">
						<p class="subtitle">PEDIDO ELETRÔNICO</p>
						<p>Oferecemos o sistema de pedido eletrônico, levando você á nossa empresa, tendo acesso rápido e ágil ao nosso estoque 24 hs, com diversas promoções, descontos e informações de forma simples, inclusive poderá acessar seus últimos eletrônico.</p>
					</div>
					<div class="col s12 m6 l3">
						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/operadores.png" alt="Operadores" class="responsive-img">
						<p class="subtitle">OPERADORES LOGÍSTICOS</p>
						<p>Temos parceria com diversos laboratórios/indústrias, juntamente com suas equipes. Gerando benefícios e melhores condições.Nos consulte.</p>
					</div>
				</div>
			</div>
		</section>

		<section class="container marcas interna">
				<h1 class="title"><span>FORNECEDORES</span></h1>

				<?php
			    # Fornecedores
			    $fornecedores = new WP_Query(
			        array(
			            'post_type' => 'fornecedor',
			            'posts_per_page' => -1
			        )
			    );

			    if ($fornecedores->have_posts()) :
			    ?>

				<div class="container-marcas">
                    <a href="javascript:;" class="arrow left"><i class="fa fa-angle-left"></i></a>

                    <div class="template-marcas cycle-slideshow" data-cycle-swipe="true" data-cycle-swipe-fx="scrollHorz" data-cycle-fx="carousel" data-cycle-timeout="2000" data-cycle-slides=".item" data-cycle-next=".marcas .right" data-cycle-prev=".marcas .left">
                    	<?php
			            while($fornecedores->have_posts()) :
			                $fornecedores->the_post();

			            	// get custom fields
			            	$id = get_the_id();
			             	$link = get_field('link');
			            ?>

						<a href="<?php echo $link ?>" target="blank" class="item">
							<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($id) );?>" alt="<?php echo get_the_title();?>">
						</a>

						<?php endwhile; ?>
                    </div>
				<?php endif; ?>
                    <a href="javascript:;" class="arrow right"><i class="fa fa-angle-right"></i></a>
                </div>
		</section>


	</main>

<?php get_footer(); ?>
