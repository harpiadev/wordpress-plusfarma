<?php get_header(); ?>

	<main class="formulario interna" id="fale-conosco">
		<div class="container interna">
			<h1 class="title"><span><?php the_title(); ?></span></h1>

			<p class="subtitulo">Telefones</p>
			<ul class="list">
				<li><span>PLUSFARMA-CE - 0800.724.3800</span></li>
				<li><span>PLUSFARMA-PI - 0800.086.4800</span></li>
			</ul>

			<p class="subtitulo">Informe os seus dados</p>

			<?php echo do_shortcode('[contact-form-7 id="76" title="Fale Conosco"]') ?>

			<div id="mapa">
				<div class="row">
					<div class="col s12 m6 lg6">
						<p class="subtitulo">Localização</p>
						<p class="mapa-endereco">Rua Barão do Rio Branco, 28 - Pan Americano, Fortaleza - CE</p>
					</div>
					<div class="col s12 m6 lg6">
						<div class="input-field">
							<select class="mapa-cidades">
									<option value="0">FORTALEZA - CE</option>
									<option value="1">TERESINA - PI</option>
							</select>
						</div>
					</div>
				</div>
				<div>
					<iframe class="mapa-google" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.3376318210567!2d-38.533536485241044!3d-3.736401897281614!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c74901241c82f5%3A0x5c7217b668205dce!2sPlusfarma+Produtos+Farmac%C3%AAuticos!5e0!3m2!1spt-BR!2sbr!4v1479841082352" width="970" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</main>

<?php get_footer(); ?>
