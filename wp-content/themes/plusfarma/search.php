	<?php get_header(); ?>

	<main id="search-results" >
		<div class="container interna">

			<?php
		    if (have_posts()) :
		    ?>
			<h1 class="text-center">Resultados para '<?php the_search_query(); ?>'</h1>


			<div class="row list">
				<?php
				while (have_posts()) :
					the_post();
				?>
				<div class="col s12 m6">
					<a href="<?php the_permalink();?>">
						<h3><?php the_title();?></h3>
					</a>
					<p><?php echo get_excerpt(100);?></p>

					<?php if (in_array(get_post_type(), array('post', 'page'))) : ?>
						<a href="<?php the_permalink();?>" class="btn">Saiba mais</a>
					<?php else : ?>
						<p class="nomargin"><?php echo ucfirst(get_post_type());?></p>
					<?php endif; ?>
				</div>
				<?php endwhile; ?>
			</div>
			<!-- /#noticias -->
			<?php else : ?>
				<h1>Nenhum resultado para '<?php the_search_query(); ?>'.</h1>
			<?php endif; ?>
		</div>
	</main>

	<?php get_footer(); ?>